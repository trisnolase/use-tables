<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table  = 'products';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
}
