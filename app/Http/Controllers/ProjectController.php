<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        $categories = [
            ["id" => 1, "name" => "Electronics"],
            ["id" => 2, "name" => "Clothing"],
            ["id" => 3, "name" => "Books"],
            ["id" => 4, "name" => "Home & Garden"],
            ["id" => 5, "name" => "Sports & Outdoors"],
            ["id" => 6, "name" => "Toys & Games"],
            ["id" => 7, "name" => "Automotive"],
            ["id" => 8, "name" => "Health & Beauty"],
            ["id" => 9, "name" => "Jewelry"],
            ["id" => 10, "name" => "Food & Beverages"]
        ];

        // $canInfo = 1;
        // $canEdit = 1;

        // return view('projects.index', compact('categories', 'canInfo', 'canEdit'));
        return view('projects.index', compact('categories'));
    }

    public function getProjects(Request $request)
    {
        $category = $request->input('category');
        $query = Project::select('*');

        if ($category) {
            $query->where('category_id', $category);
        }

        $draw = $request->input('draw');
        $start = $request->input('start');
        $length = $request->input('length');

        $recordsTotal = Project::count(); // total records without filtering
        $recordsFiltered = $query->count(); // total records with filtering

        $data = $query->skip($start)->take($length)->get();

        // Build the actions column HTML
        $actions = [];
        foreach ($data as $project) {
            $canInfo = true; // replace this logic with your logic 
            $canEdit = true; // replace this logic with your logic
            $project->canInfo = $canInfo;
            $project->canEdit = $canEdit;
            $actions[] = $project;
        }

        $response = [
            'draw' => (int)$draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $data,
            'actions' => $actions, // include the actions column
        ];

        return response()->json($response);
    }
}
