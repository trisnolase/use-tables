@extends('layouts')

@section('content')
    <div class="col">
        <h1>Product List</h1>
        <select id="categorySelect" class="form-control col-2 mb-4">
            <option value="">All Categories</option>
            @foreach ($categories as $category)
                <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
            @endforeach
        </select>
        <div class="table-responsive">
            <table class="table table-hover" id="projectTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            var dataTable = $('#projectTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route('getProjects') }}',
                    type: 'GET',
                    data: function(d) {
                        d.category = $('#categorySelect').val();
                    },
                },
                columns: [{
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'description',
                        name: 'description'
                    },
                    {
                        data: 'price',
                        name: 'price'
                    },
                    {
                        data: 'stock',
                        name: 'stock'
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            var actionsHtml = '';
                            if (data.canEdit) {
                                actionsHtml += '<a class="btn btn-sm btn-primary mr-2" href="/edit?' + row.id + '" class="edit-button">Edit</a>'; //replace this code with actual route
                            }
                            if (data.canInfo) {
                                actionsHtml += '<a class="btn btn-sm btn-info" href="/info?' + row.id + '" class="info-button">Info</a>'; //replace this code with actual route
                            }
                            return actionsHtml;
                        }
                    }
                ]
            });

            $('#categorySelect').on('change', function() {
                dataTable.ajax.reload();
            });
        });
    </script>
@endpush
